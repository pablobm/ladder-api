# Ladder API

The Thoughtbot London (nee New Bamboo) Table Tennis Ladder. Its only interface uses [JSON-API](http://www.json-api.org), and therefore you may want to use a frontend too. One such can be found at https://bitbucket.org/pablobm/ladder-client-ember.

## Prerequisites

To build this project, you'll need Ruby and Bundler.

## Installation

  * Fork this repository and `git clone` it
  * Change into the new directory
  * Run `bundle install`
  * Copy `.env.example` to `.env`
  * Run `./bin/rake db:migrate`

A local setup uses SQLite, so you won't need to set up a database. The included `config/database.yml` will work.

## Running the app

This app runs Puma, which is a bit less flexible about address binding than you may be used to. Typically, you'll want to run the app locally using the following command:

  ./bin/rails s -b 0.0.0.0

This explicitly binds to `0.0.0.0`, as opposed to `localhost`. This works better with Ember's proxying capabilities.

## Running the tests

This app uses RSpec. Run the tests with `bundle exec rake`.

## Deploying

This app is deployed to Heroku, with name `ladder-api`. Ask Pablo (@thoughtbot.com) if you wish to have access.

## Configuration variables

Configuration variables live at `.env`, and are as follows:

  * `TRUSTED_CLIENT_IPS`: a comma-separated list of IPv4 addresses. Only clients connecting from these will be "trusted", meaning they have read-write access. Any non-trusted clients will have read-only access. By default, all clients are trusted.
