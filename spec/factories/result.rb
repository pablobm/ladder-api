FactoryGirl.define do
  factory :result do
    log [10, -8]
    association :winner, factory: :player
    association :loser, factory: :player
  end
end
