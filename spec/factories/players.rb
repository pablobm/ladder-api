FactoryGirl.define do
  factory :player do
    sequence(:name){|i| "Alice #{i}" }
    active true
  end
end
