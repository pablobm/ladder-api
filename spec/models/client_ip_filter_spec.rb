require 'models_helper'

describe ClientIPFilter do
  def test_filter(filter, remote_ip)
    request = double(remote_ip: remote_ip)
    filter.trusted?(request)
  end

  it "accepts comma-separated IP addresses" do
    filter = ClientIPFilter.new('1.2.3.4, 2.3.4.5')
    expect(test_filter(filter, '1.2.3.4')).to eq(true)
    expect(test_filter(filter, '2.3.4.5')).to eq(true)
    expect(test_filter(filter, '4.3.2.1')).to eq(false)
  end

  it "accepts a wildcard" do
    filter = ClientIPFilter.new('*')
    expect(test_filter(filter, '1.2.3.4')).to eq(true)
    expect(test_filter(filter, '2.3.4.5')).to eq(true)
    expect(test_filter(filter, '4.3.2.1')).to eq(true)
  end

  it "does not accept nil" do
    expect {
      ClientIPFilter.new(nil)
    }.to raise_error(ArgumentError)
  end

  it "does not accept an empty string" do
    expect {
      ClientIPFilter.new('')
    }.to raise_error(ArgumentError)
  end
end
