require 'models_helper'

RSpec.describe Result, type: :model do
  describe "time-related scopes" do
    let!(:r1) { FactoryGirl.create(:result, created_at: 1.month.ago) }
    let!(:r2) { FactoryGirl.create(:result, created_at: 1.day.ago) }
    let!(:r3) { FactoryGirl.create(:result, created_at: 1.week.ago) }
    let!(:r4) { FactoryGirl.create(:result, created_at: 1.month.ago) }
    let!(:r5) { FactoryGirl.create(:result, created_at: 1.month.ago) }

    describe ".latest" do
      it "returns all results, newest first" do
        expect(Result.latest).to eq([r2, r3, r5, r4, r1])
      end
    end

    describe ".after" do
      it "returns results that took place from the given one, oldest first" do
        expect(Result.after(r4)).to eq([r5, r3, r2])
      end
    end
  end

  describe "#winner" do
    it "must be present" do
      attrs = FactoryGirl.attributes_for(:result)
      attrs.delete(:winner)
      result = Result.new(attrs)
      expect(result).not_to be_valid
      expect(result.errors[:winner]).not_to be_empty
    end
  end

  describe "#loser" do
    it "must be present" do
      attrs = FactoryGirl.attributes_for(:result)
      attrs.delete(:loser)
      result = Result.new(attrs)
      expect(result).not_to be_valid
      expect(result.errors[:loser]).not_to be_empty
    end
  end

  describe "#winner and #loser" do
    let(:player1) { FactoryGirl.build_stubbed(:player) }
    let(:player2) { FactoryGirl.build_stubbed(:player) }

    it "can't be the same player" do
      attrs = FactoryGirl.attributes_for(:result)
      attrs[:loser] = player1
      attrs[:winner] = player1
      result = Result.new(attrs)
      expect(result).not_to be_valid
      expect(result.errors[:base]).not_to be_empty
    end
  end

  describe ".oldest" do
    let!(:r1) { FactoryGirl.create(:result, created_at: 1.month.ago) }
    let!(:r2) { FactoryGirl.create(:result, created_at: 1.day.ago) }
    let!(:r3) { FactoryGirl.create(:result, created_at: 1.week.ago) }
    let!(:r4) { FactoryGirl.create(:result, created_at: 1.month.ago) }
    let!(:r5) { FactoryGirl.create(:result, created_at: 1.month.ago) }

    it "returns all results, olders first" do
      expect(Result.oldest).to eq([r1, r4, r5, r3, r2])
    end
  end

  describe "#set_log" do
    subject(:result) { FactoryGirl.create(:result, log: [10, -5]) }

    it "updates the `log` field" do
      expect(result.log).to eql([10, -5])
      result.set_log([8, -8])
      expect(result.log).to eql([8, -8])
      result.reload
      expect(result.log).to eql([8, -8])
    end
  end
end
