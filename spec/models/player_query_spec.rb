require 'models_helper'

RSpec.describe PlayerQuery, type: :model do
  describe ".new_from_params" do
    it "parses the `active` param" do
      pq = PlayerQuery.new_from_params({})
      expect(pq.params.active).to eql(true)

      pq = PlayerQuery.new_from_params(active: 'true')
      expect(pq.params.active).to eql(true)

      pq = PlayerQuery.new_from_params(active: 'false')
      expect(pq.params.active).to eql(false)

      pq = PlayerQuery.new_from_params(active: '*')
      expect(pq.params.active).to eql(PlayerQuery.any)
    end
  end

  describe "#results" do
    describe "by default" do
      let!(:p1) { FactoryGirl.create(:player) }
      let!(:p2) { FactoryGirl.create(:player, active: false) }
      let!(:p3) { FactoryGirl.create(:player) }

      it "returns only active players" do
        actual = PlayerQuery.new.results
        expected = [p1, p3]
        expect(actual).to match(expected)
      end
    end

    describe "active: any" do
      let!(:p1) { FactoryGirl.create(:player) }
      let!(:p2) { FactoryGirl.create(:player, active: false) }
      let!(:p3) { FactoryGirl.create(:player) }

      it "returns all players" do
        actual = PlayerQuery.new(active: PlayerQuery.any).results
        expected = [p1, p2, p3]
        expect(actual).to match(expected)
      end
    end
  end
end
