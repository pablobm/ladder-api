require 'models_helper'

RSpec.describe EloScorer, type: :model do
  describe "#update_scores" do
    let(:p1) { spy(:winner, score: 1000) }
    let(:p2) { spy(:loser, score: 1200) }
    subject(:scorer) { EloScorer.new }

    it "transfers points from loser to winner" do
      scorer.update_scores(winner: p1, loser: p2)
      expect(p1).to have_received(:update_score!).with(1019)
      expect(p2).to have_received(:update_score!).with(1181)
    end

    class MockPlayer
      attr_reader :increments
      attr_accessor :score

      def initialize
        @increments = []
      end

      def update_score!(score)
        @increments << score - self.score
      end
    end

    it "transfers points fewer points the more likely the victory" do
      p1 = MockPlayer.new

      winner_scores = (800..2500).step(10)
      loser_score = 1200
      winner_scores.each do |winner_score|
        p1.score = winner_score
        p2.score = loser_score
        scorer.update_scores(winner: p1, loser: p2)
      end

      transfers = p1.increments
      expect(transfers.count).to eq(171)
      transfers.each do |transfer|
        expect(transfer).to be_kind_of(Integer)
      end
      expect(transfers.sort).to eq(transfers.reverse)
      expect(transfers.first).to satisfy("be greater than #{transfers.last}"){|n| transfers.last < n }
    end

    it "does not remove points under a lower limit" do
      scorer = EloScorer.new(floor: p2.score)
      scorer.update_scores(winner: p1, loser: p2)
      expect(p2).to have_received(:update_score!).with(p2.score)
    end

    it "passes the transferred amounts to a block" do
      tw, tl = nil
      initial1, initial2 = p1.score, p2.score
      scorer = EloScorer.new
      scorer.update_scores(winner: p1, loser: p2){|t| tw, tl = t }
      expect(p1).to have_received(:update_score!).with(initial1 + tw)
      expect(p2).to have_received(:update_score!).with(initial2 + tl)
    end
  end
end
