require 'models_helper'

RSpec.describe ScoringResult, type: :model do
  context do
    subject { ScoringResult.new(Result.new, double) }
    it_behaves_like "ActiveModel"
  end

  describe ".model_name" do
    it "is a valid ActiveModel `model_name`, passing for a Result" do
      mn = ScoringResult.model_name
      expect(mn.name).to eql('Result')
      expect(mn.route_key).to eql('results')
    end
  end

  describe "#save" do
    let(:winner) { spy }
    let(:loser) { spy }
    let(:result) { spy(winner: winner, loser: loser) }
    let(:scorer) { spy }
    subject(:sr) { ScoringResult.new(result, scorer) }

    context "on success" do
      it "returns `true`" do
        expect(sr.save).to eq(true)
      end

      it "saves the result" do
        sr.save
        expect(result).to have_received(:save)
      end

      it "alters the players' scores" do
        sr.save
        expect(scorer).to have_received(:update_scores).with(winner: winner, loser: loser)
      end

      it "remembers the transferred amounts" do
        allow(scorer).to receive(:update_scores).and_yield([10, -8])
        sr.save
        expect(result).to have_received(:set_log).with([10, -8])
      end
    end

    context "on failure" do
      before do
        allow(result).to receive(:save).and_return(false)
      end

      it "returns `false`" do
        expect(sr.save).to eq(false)
      end

      it "does not alter scores if saving fails" do
        sr.save
        expect(scorer).not_to have_received(:update_scores)
      end
    end
  end

  describe "#destroy" do
    let!(:p1) { FactoryGirl.create(:player, name: "1", score: 800) }
    let!(:p2) { FactoryGirl.create(:player, name: "2", score: 900) }
    let!(:p3) { FactoryGirl.create(:player, name: "3", score: 1200) }

    let!(:pa) { FactoryGirl.create(:player, name: "A", score: 800) }
    let!(:pb) { FactoryGirl.create(:player, name: "B", score: 900) }
    let!(:pc) { FactoryGirl.create(:player, name: "C", score: 1200) }

    def score(winner, loser)
      Result.new(winner: winner, loser: loser).tap do |r|
        ScoringResult.new(r).save!
      end
    end

    it "destroys the result" do
      score(p1, p2)
      score(p2, p3)
      score(p2, p1)
      r4 = score(p1, p3)
      score(p3, p2)
      score(p2, p1)

      sr4 = ScoringResult.new(r4)
      sr4.destroy
      expect {
        r4.reload
      }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it "recalculates players' scores" do
      scores_of = ->(*xs){ xs.map{|x| x.reload.score } }

      score(p1, p2)
      score(p2, p3)
      score(p2, p1)
      r4 = score(p1, p3)
      score(p1, p2)
      score(p2, p1)

      sr4 = ScoringResult.new(r4)
      sr4.destroy
      score(pa, pb)
      score(pb, pc)
      score(pb, pa)
      score(pa, pb)
      score(pb, pa)

      expect(scores_of.(p1, p2, p3)).to eq(scores_of.(pa, pb, pc))
    end
  end
end
