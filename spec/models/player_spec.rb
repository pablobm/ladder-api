require 'models_helper'

RSpec.describe Player, type: :model do
  describe ".active" do
    let!(:p1) { FactoryGirl.create(:player) }
    let!(:p2) { FactoryGirl.create(:player, active: false) }
    let!(:p3) { FactoryGirl.create(:player) }

    it "returns only active players" do
      expect(Player.active).to match([p1, p3])
    end
  end

  describe ".inactive" do
    let!(:p1) { FactoryGirl.create(:player) }
    let!(:p2) { FactoryGirl.create(:player, active: false) }
    let!(:p3) { FactoryGirl.create(:player) }

    it "returns only inactive players" do
      expect(Player.inactive).to match([p2])
    end
  end

  describe "by default" do
    subject(:player) { Player.new }

    it "is active" do
      expect(player.active).to eql(true)
    end
  end

  describe "#name" do
    it "is required" do
      attrs = FactoryGirl.attributes_for(:player)
      attrs.delete(:name)
      player = Player.new(attrs)
      expect(player).not_to be_valid
      expect(player.errors[:name]).not_to be_empty
    end
  end

  describe "#score" do
    it "is set to the default" do
      attrs = FactoryGirl.attributes_for(:player)
      attrs.delete(:score)
      player = Player.new(attrs)
      player.save
      expect(player.score).to eq(Config.scorer.initial_score)
    end
  end

  describe "#update_score!" do
    it "updates the score and stores it" do
      player = FactoryGirl.create(:player, score: 1000)
      player.update_score!(900)
      player.reload
      expect(player.score).to eq(900)
    end
  end
end
