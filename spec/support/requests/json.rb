module RequestsHelper
  module Json
    def json
      JSON.parse(response.body)
    end

    def json_data
      json.fetch('data')
    end

    def json_included
      json.fetch('included')
    end

    def json_links
      json.fetch('links')
    end

    def json_errors
      json.fetch('errors')
    end

    def json_errors?
      !!json.fetch('errors'){ false }
    end
  end
end
