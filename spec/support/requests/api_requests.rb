module RequestsHelper
  module ApiRequests
    def api_delete(path)
      send(:delete, path)
    end

    def api_post(path, type, attributes, relationships = :no_relationships)
      api_request(:post, path, type, attributes, relationships)
    end

    def api_patch(path, type, attributes, relationships = :no_relationships)
      api_request(:patch, path, type, attributes, relationships)
    end

    private

    def api_request(method, path, type, attributes, relationships)
      api_type = type.to_s.pluralize
      data = {
        type: api_type,
        attributes: attributes,
      }
      unless relationships == :no_relationships
        data[:relationships] = relationships
      end
      json = JSON.dump({
        data: data,
      })
      send(method, path, json, {
        'Content-Type' => 'application/vnd.api+json',
      })
    end
  end
end
