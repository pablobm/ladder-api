module RequestsHelper
  module Authentication
    def trust_requests!
      allow(Config.request_filter).to receive(:trusted?).and_return(true)
    end

    def do_not_trust_requests!
      allow(Config.request_filter).to receive(:trusted?).and_return(false)
    end
  end
end
