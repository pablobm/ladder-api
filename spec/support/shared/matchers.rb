module SharedHelper
  module Matchers
    def a_time
      a_string_matching(%r{\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d+\.\d+Z})
    end

    def an_id_string
      a_string_matching(/^[0-9]+$/)
    end

    def an_integer
      be_a_kind_of(Fixnum)
    end

    def an_integer_id
      an_integer
    end

    def increase(*args, &block)
      change(*args, &block).by_at_least(1)
    end

    def decrease(*args, &block)
      change(*args, &block).by_at_most(-1)
    end
  end
end
