require 'rails_helper'

Dir[Rails.root.join('spec/support/requests/**/*.rb')].each { |f| require f }

RSpec.configure do |config|
  modules = [
    RequestsHelper::ApiRequests,
    RequestsHelper::Authentication,
    RequestsHelper::Json,
  ]
  modules.each do |mod|
    config.include mod, type: :request
    config.before type: :request do
      trust_requests!
    end
  end
end
