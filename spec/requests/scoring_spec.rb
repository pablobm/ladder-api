require 'requests_helper'

RSpec.describe "Scoring", type: :request do
  describe "on new result" do
    let!(:p1) { FactoryGirl.create(:player, score: 1000) }
    let!(:p2) { FactoryGirl.create(:player, score: 1000) }

    def create_result(winner:, loser:)
      api_post results_path, :result, nil, {
        winner: {
          data: { type: 'players', id: winner.id.to_s },
        },
        loser: {
          data: { type: 'players', id: loser.id.to_s },
        },
      }
    end

    def score_of(player)
      get player_path(player)
      json_data['attributes']['score']
    end

    it "changes the players' scores" do
      expect {
        create_result(winner: p1, loser: p2)
      }.to  increase{ score_of(p1) }
       .and decrease{ score_of(p2) }
    end
  end
end
