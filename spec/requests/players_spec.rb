require 'requests_helper'

RSpec.describe "Players", type: :request do
  describe "GET /players" do
    let!(:p1) { FactoryGirl.create(:player) }
    let!(:p2) { FactoryGirl.create(:player) }
    let!(:p3) { FactoryGirl.create(:player, active: false) }

    it "returns all active players" do
      get players_path
      expect(json_data.length).to eql(2)
    end

    describe "?active=*" do
      it "returns all players" do
        get players_path, active: '*'
        expect(json_data.length).to eql(3)
      end
    end

    it "is allowed to all clients" do
      do_not_trust_requests!
      get players_path
      expect(json_errors?).to eq(false)
    end
  end

  describe "POST /players" do
    def perform!
      api_post players_path, :player, name: "Alice"
    end

    it "creates a new player" do
      expect {
        perform!
      }.to change{ Player.count }.by(1)
    end

    it "returns the newly created player" do
      perform!
      expect(json_data).to match({
        id: an_id_string,
        type: 'players',
        attributes: {
          name: "Alice",
          score: 800,
        }
      }.deep_stringify_keys)
    end

    it "is allowed only to trusted clients" do
      do_not_trust_requests!
      perform!
      expect(json_errors?).to eq(true)
    end

    context "when there is an error" do
      before do
        api_post players_path, :player, name: ''
      end

      it "returns an error status" do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "describes the problem" do
        error = json_errors.first
        expect(error.fetch('detail')).to match(/blank/)
      end
    end
  end

  describe "PATCH /players" do
    let!(:player) { FactoryGirl.create(:player, name: "Alice") }

    def perform!(attrs = {name: "Alicia"})
      api_patch player_path(player), :player, attrs
    end

    it "updates the given player" do
      expect {
        perform!
      }.to change{ player.reload.name }.from("Alice").to("Alicia")
    end

    it "returns the updated player" do
      perform!
      expect(json_data).to match({
        id: an_id_string,
        type: 'players',
        attributes: {
          name: "Alicia",
          score: 800,
        }
      }.deep_stringify_keys)
    end

    it "is allowed only to trusted clients" do
      do_not_trust_requests!
      perform!
      expect(json_errors?).to eq(true)
    end

    context "when there is an error" do
      before do
        api_patch player_path(player), :player, name: ''
      end

      it "returns an error status" do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "describes the problem" do
        error = json_errors.first
        expect(error.fetch('detail')).to match(/blank/)
      end
    end
  end
end
