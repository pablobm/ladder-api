require 'requests_helper'

RSpec.describe "Results", type: :request do
  describe "GET /results" do
    let!(:p1) { FactoryGirl.create(:player) }
    let!(:p2) { FactoryGirl.create(:player) }
    let!(:p3) { FactoryGirl.create(:player) }
    let!(:r1) { FactoryGirl.create(:result, winner: p1, loser: p2, created_at: 1.month.ago) }
    let!(:r2) { FactoryGirl.create(:result, winner: p2, loser: p1, created_at: 1.day.ago) }
    let!(:r3) { FactoryGirl.create(:result, winner: p1, loser: p2, created_at: 1.week.ago) }
    let!(:r4) { FactoryGirl.create(:result, winner: p1, loser: p2, created_at: 1.month.ago) }
    let!(:r5) { FactoryGirl.create(:result, winner: p1, loser: p2, created_at: 1.month.ago) }

    it "lists existing results, newest first" do
      get results_path
      actual_ids = json_data.map{|e| e['id'] }
      expect(actual_ids).to eq([r2.id, r3.id, r5.id, r4.id, r1.id].map(&:to_s))
    end

    it "includes any involved players" do
      get results_path
      actual_incs = json_included.map{|e| e['id'] }
      expect(actual_incs).to match_array([p1.id, p2.id].map(&:to_s))
    end

    it "paginates the results" do
      allow(Config).to receive(:result_page_size).and_return(2)

      get results_path
      expect(json_data.count).to eq(2)
      expect(json_links.keys).to match_array(%w{self next last})

      get results_path, page: { number: 2 }
      expect(json_data.count).to eq(2)
      expect(json_links.keys).to match_array(%w{first prev self next last})

      get results_path, page: { number: 3 }
      expect(json_data.count).to eq(1)
      expect(json_links.keys).to match_array(%w{first prev self})
    end

    it "is allowed to all clients" do
      do_not_trust_requests!
      get results_path
      expect(json_errors?).to eq(false)
    end
  end

  describe "POST /results" do
    let!(:p1) { FactoryGirl.create(:player) }
    let!(:p2) { FactoryGirl.create(:player) }

    def perform!(winner: p1, loser: p2)
      api_post results_path, :result, nil, {
        winner: {
          data: { type: 'players', id: winner.id.to_s },
        },
        loser: {
          data: { type: 'players', id: loser.id.to_s },
        },
      }
    end

    it "returns the newly created result" do
      perform!
      expect(json_data).to match(a_hash_including({
        id: an_id_string,
        type: 'results',
        attributes: {
          transfer: an_integer,
          created_at: a_time,
        },
        relationships: {
          winner: {
            data: { type: 'players', id: p1.id.to_s },
          },
          loser: {
            data: { type: 'players', id: p2.id.to_s },
          },
        },
      }.deep_stringify_keys))
    end

    it "includes details of involved players" do
      perform!
      expect(json_included).to match([
        { type: 'players', id: p1.id.to_s, attributes: { name: p1.name, score: an_integer } },
        { type: 'players', id: p2.id.to_s, attributes: { name: p2.name, score: an_integer } },
      ].map(&:deep_stringify_keys))
    end

    it "is allowed only to trusted clients" do
      do_not_trust_requests!
      perform!
      expect(json_errors?).to eq(true)
    end

    context "when there is an error" do
      before do
        perform!(winner: p1, loser: p1)
      end

      it "returns an error status" do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "describes the problem" do
        error = json_errors.first
        expect(error.fetch('detail')).to match(/cannot be the same/)
      end
    end
  end

  describe "DELETE /results/:id" do
    let!(:p1) { FactoryGirl.create(:player) }
    let!(:p2) { FactoryGirl.create(:player, score: 1000) }
    let!(:p3) { FactoryGirl.create(:player, score: 1200) }

    let!(:r1) { score(p1, p2) }
    let!(:r2) { score(p2, p3) }
    let!(:r3) { score(p2, p1) }
    let!(:r4) { score(p1, p3) }
    let!(:r5) { score(p3, p2) }
    let!(:r6) { score(p2, p1) }

    def score(winner, loser)
      Result.new(winner: winner, loser: loser).tap do |r|
        ScoringResult.new(r).save!
      end
    end

    def perform!(result)
      api_delete result_path(result)
    end

    context "with a recent result" do
      before do
        perform!(r6)
      end

      it "returns a success response" do
        expect(response).to have_http_status(204)
      end

      it "recalculates scores" do
        new_scores = [p1, p2, p3].each(&:reload).map(&:score)
        expect(new_scores).to eq([835, 1000, 1165])
      end
    end
  end
end
