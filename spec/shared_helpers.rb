require 'rails_helper'

Dir[Rails.root.join('spec/support/shared/**/*.rb')].each { |f| require f }

RSpec.configure do |config|
  config.include SharedHelper::Matchers
end
