require 'sequel'
require 'pp'

if ARGV.count != 2
  puts "Usage: #{$PROGRAM_NAME} OLD_DB_URL NEW_DB_URL"
  exit 1
end

OLD_DB_URL = ARGV[0]
NEW_DB_URL = ARGV[1]

old_db = Sequel.connect(OLD_DB_URL)
new_db = File.exist?(NEW_DB_URL) ? Sequel.sqlite(NEW_DB_URL) : Sequel.connect(NEW_DB_URL)

old_players = old_db[:players]
new_players = new_db[:players]
new_players.delete
old_players.each do |old|
  new_players.insert({
    id: old[:id],
    name: old[:name],
    score: old[:elo_rating],
    created_at: old[:created_at],
    updated_at: Time.now,
  })
end
new_db.reset_primary_key_sequence(:players)

old_results = old_db[:results]
new_results = new_db[:results]
new_results.delete
old_results.each do |old|
  new_results.insert({
    id: old[:id],
    winner_id: old[:winner_id],
    loser_id: old[:loser_id],
    created_at: old[:created_at],
    updated_at: Time.now,
  })
end
new_db.reset_primary_key_sequence(:results)
