namespace :ladder do
  desc "Recalculate scores based on all recorded results"
  task rerun: :environment do
    Player.transaction do
      Player.update_all(score: Config.scorer.initial_score)
      ScoringResult.replay(Result.oldest)
    end
  end

  desc "Update the `active` flag of all players"
  task update_active: :environment do
    Player.find_each do |player|
      latest = Result.latest.where(%{loser_id = :player_id OR winner_id = :player_id}, player_id: player.id).first
      next unless latest
      player.active = latest.created_at && latest.created_at > 1.month.ago
      player.save
    end
  end
end
