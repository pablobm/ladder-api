class AddTransferToResults < ActiveRecord::Migration
  def change
    add_column :results, :transfer, :integer
  end
end
