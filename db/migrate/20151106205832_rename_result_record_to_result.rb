class RenameResultRecordToResult < ActiveRecord::Migration
  def change
    rename_table :result_records, :results
  end
end
