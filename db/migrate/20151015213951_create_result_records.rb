class CreateResultRecords < ActiveRecord::Migration
  def change
    create_table :result_records do |t|
      t.belongs_to :winner, index: true, foreign_key: true
      t.belongs_to :loser, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
