class ChangeTransferToLog < ActiveRecord::Migration
  def change
    change_table :results do |t|
      t.remove :transfer
      t.text :log
    end
  end
end
