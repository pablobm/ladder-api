Rails.application.routes.draw do
  resources :players, only: [:index, :show, :create, :update]
  resources :results, only: [:index, :show, :create, :destroy]
end
