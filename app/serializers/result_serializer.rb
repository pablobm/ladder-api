class ResultSerializer < ActiveModel::Serializer
  attributes :id, :transfer, :created_at

  belongs_to :winner
  belongs_to :loser
end
