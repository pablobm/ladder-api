class EloScorer
  DEFAULT_FLOOR = 800
  DEFAULT_INITIAL_SCORE = 800

  attr_reader :floor, :initial_score

  def initialize(floor: DEFAULT_FLOOR, initial_score: DEFAULT_INITIAL_SCORE)
    @floor = floor
    @initial_score = initial_score
  end

  def update_scores(winner:, loser:, &block)
    winner_initial = winner.score
    loser_initial  = loser.score

    block = Proc.new{} unless block
    w = Elo::Player.new(rating: winner.score)
    l = Elo::Player.new(rating: loser.score)

    w.wins_from(l)
    transfer = loser_initial - l.rating

    winner_new_score = winner_initial + transfer
    winner.update_score!(winner_new_score)

    loser_new_score = [
      loser_initial - transfer,
      floor,
    ].max
    loser.update_score!(loser_new_score)

    log = [
      winner_new_score - winner_initial,
      loser_new_score - loser_initial,
    ]

    block.call(log)
    nil
  end

  def undo_score(winner:, loser:, log:)
    tw, tl = log

    Player.transaction do
      winner_new_score = winner.score - tw
      winner.update_score!(winner_new_score)

      loser_new_score = loser.score - tl
      loser.update_score!(loser_new_score)
    end
  end
end
