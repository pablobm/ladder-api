class Player < ActiveRecord::Base
  before_validation :ensure_score

  after_initialize :set_defaults

  validates :name, presence: true
  validates :active, inclusion: { in: [true, false] }

  def self.active
    where(active: true)
  end

  def self.inactive
    where(active: false)
  end

  def update_score!(score)
    self.update_attributes!(score: score)
  end

  private

  def ensure_score
    self.score = Config.scorer.initial_score unless self.score
  end

  def set_defaults
    if ![true, false].include?(active)
      self.active = true
    end
  end
end
