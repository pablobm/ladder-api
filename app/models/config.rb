module Config
  def self.scorer
    EloScorer.new(
      floor: 800,
      initial_score: 800,
    )
  end

  def self.result_page_size
    25
  end

  def self.request_filter
    trusted_ips = ENV.fetch('TRUSTED_CLIENT_IPS'){ '*' }
    @request_filter ||= ClientIPFilter.new(trusted_ips)
  end
end
