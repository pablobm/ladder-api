class PlayerQuery
  class << self
    def new_from_params(params)
      query = {}
      if params.key?(:active)
        pa = params[:active]
        query[:active] = if pa == '*'
          any
        else
          booleanize(pa)
        end
      end

      new(query)
    end

    def any
      Any
    end

    private

    module Any; end

    def booleanize(param)
      case param
      when true, 1, 'true', '1'
        true
      else
        false
      end
    end
  end

  attr_reader :params

  def initialize(opts = {})
    @params = Params.new(opts)
  end

  def results
    scope = Player.all

    case params.active
    when true
      scope = Player.active
    when false
      scope = Player.inactive
    end

    scope
  end

  private

  class Params
    attr_reader :active

    def initialize(opts = {})
      @active = opts.fetch(:active){ true }
    end
  end
end
