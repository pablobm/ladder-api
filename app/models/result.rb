class Result < ActiveRecord::Base
  belongs_to :winner, class_name: Player
  belongs_to :loser, class_name: Player

  validates_presence_of :winner, :loser
  validate :winner_and_loser_cannot_be_the_same

  serialize :log, JSON

  def self.latest
    order('created_at DESC, id DESC')
  end

  def self.oldest
    order(:created_at, :id)
  end

  def self.after(other)
    latest.where('created_at >= ?', other.created_at)
      .reverse
      .drop_while{|r| r.id < other.id}
      .drop(1)
  end

  def set_log(t)
    update_attributes!(log: t)
  end

  def transfer
    log.max
  end

  private

  def winner_and_loser_cannot_be_the_same
    if winner.present? && loser.present? && winner == loser
      errors.add(:base, "Winner and loser cannot be the same player")
    end
  end
end
