class ClientIPFilter

  def initialize(filter_str)
    if filter_str == '' || filter_str.nil?
      raise ArgumentError, "#{self.class.name} must be initialized with a String of comma-separated IP addresses or `'*'`"
    end

    @trusted = filter_str.split(',').map(&:strip)
  end

  def trusted?(request)
    if @trusted == ['*']
      true
    else
      @trusted.include?(request.remote_ip)
    end
  end

end
