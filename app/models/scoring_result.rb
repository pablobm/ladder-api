class ScoringResult

  include ActiveModel::Model
  include ActiveModel::Serializers

  def self.find(id)
    ScoringResult.new(Result.find(id))
  end

  def self.model_name
    ActiveModel::Name.new(Result)
  end

  def self.replay(results)
    results.each do |r|
      r.reload
      ScoringResult.new(r).save
    end
  end

  def self.undo_without_replay(results)
    results.each do |r|
      r.reload
      ScoringResult.new(r).undo_without_replay
    end
  end

  delegate :transfer,
           to: :result

  def initialize(result, scorer = Config.scorer)
    @result = result
    @scorer = scorer
  end

  def destroy
    Result.transaction do
      outdated_results = Result.after(result)
      self.class.undo_without_replay(([result] + outdated_results).reverse)
      result.destroy
      self.class.replay(outdated_results)
    end
  end

  def save
    Result.transaction do
      result.save or raise ActiveRecord::Rollback
      scorer.update_scores(winner: winner, loser: loser) do |log|
        result.set_log(log)
      end
      true
    end || false
  end

  def save!
    save or raise "Could not save #{result}"
  end

  def undo_without_replay
    scorer.undo_score(winner: result.winner, loser: result.loser, log: result.log)
  end

  private

  attr_reader :result, :scorer

  delegate :winner, :loser,
           to: :result
end
