class ApplicationController < ActionController::API

  private

  def trusted_request?
    Config.request_filter.trusted?(request)
  end

  def authenticate
    unless trusted_request?
      render({
        json: {
          errors: {
            text: "These are not the droids you are looking for",
          },
        },
        status: :forbidden,
      })
    end
  end

  def render_errors(model)
    render json: ErrorSerializer.serialize(model.errors), status: :unprocessable_entity
  end

end
