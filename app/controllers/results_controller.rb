class ResultsController < ApplicationController
  before_filter :authenticate, except: [:index, :show]

  def index
    results = Result.latest.page(page_number_param).per(Config.result_page_size).includes(:winner, :loser)
    render json: results, include: %w{winner loser}
  end

  def show
    render json: Result.find(params[:id]), include: %w{winner loser}
  end

  def create
    r = Result.new(result_params)
    sr = ScoringResult.new(r)
    if sr.save
      # TODO: should be `sr`, but can't get json_api to work here
      render json: r, include: %w{winner loser}
    else
      render_errors(r)
    end
  end

  def destroy
    sr = ScoringResult.find(id_param)
    if sr.destroy
      render nothing: true, status: 204
    else
      raise "Failed to destroy #{sr.inspect}"
    end
  end

  private

  def page_number_param
    params.fetch(:page){ {} }.fetch(:number){ 1 }
  end

  def id_param
    params[:id]
  end

  def result_params
    rels = params.fetch(:data).fetch(:relationships)
    {
      winner_id: rels[:winner][:data][:id],
      loser_id: rels[:loser][:data][:id],
    }
  end
end
