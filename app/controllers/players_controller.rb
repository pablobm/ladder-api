class PlayersController < ApplicationController
  before_filter :authenticate, except: [:index, :show]

  def index
    render json: PlayerQuery.new_from_params(params).results
  end

  def show
    render json: Player.find(params[:id])
  end

  def create
    player = Player.new(player_params)
    if player.save
      render json: player
    else
      render_errors(player)
    end
  end

  def update
    player = Player.find(player_id)
    if player.update_attributes(player_params)
      render json: player
    else
      render_errors(player)
    end
  end

  private

  def player_id
    params[:id]
  end

  def player_params
    params.require(:data).require(:attributes).permit(:name)
  end
end
